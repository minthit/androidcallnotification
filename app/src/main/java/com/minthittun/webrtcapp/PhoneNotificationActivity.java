package com.minthittun.webrtcapp;

import androidx.appcompat.app.AppCompatActivity;

import android.app.KeyguardManager;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.PowerManager;
import android.view.View;
import android.view.WindowManager;

import com.minthittun.webrtcapp.Helpers.SoundPoolManager;

import static com.minthittun.webrtcapp.Helpers.WebRtcAppConstant.NOTIFICATION_ID;

public class PhoneNotificationActivity extends AppCompatActivity {

    private KeyguardManager.KeyguardLock lock;
    private PowerManager.WakeLock wakeLock;
    private AudioManager audioManager;
    private SoundPoolManager soundPoolManager;

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_phone_notification);

        PowerManager pwm = (PowerManager) getSystemService(POWER_SERVICE);
        wakeLock = pwm.newWakeLock(PowerManager.FULL_WAKE_LOCK, getClass().getSimpleName());
        wakeLock.acquire();
        KeyguardManager keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
        lock = keyguardManager.newKeyguardLock(getClass().getSimpleName());
        lock.disableKeyguard();

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);


        soundPoolManager = SoundPoolManager.getInstance(this);

        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        audioManager.setSpeakerphoneOn(true);

        setVolumeControlStream(AudioManager.STREAM_VOICE_CALL);

        findViewById(R.id.btnAnswer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                soundPoolManager.release();

                final NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.cancel(NOTIFICATION_ID);

                Intent webRtcIntent = new Intent(PhoneNotificationActivity.this, WebRtcCallActivity.class);
                webRtcIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(webRtcIntent);
                finish();

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        soundPoolManager.playRinging();
    }

    @Override
    public void onPause() {
        super.onPause();

        if (wakeLock.isHeld())
            wakeLock.release();
        lock.reenableKeyguard();
    }

}