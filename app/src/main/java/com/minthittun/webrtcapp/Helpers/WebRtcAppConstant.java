package com.minthittun.webrtcapp.Helpers;

public class WebRtcAppConstant {

    public static final int NOTIFICATION_ID = 99;
    public static final String NOTIFICATION_CHANNEL_ID = "MTTFcm";
    public static final String NOTIFICATION_CHANNEL_NAME = "MTT Notification";
    public static final String CALL_ACTION_DATA = "CALL_ACTION";
    public static final String CALL_ACTION_ANSWER = "CALL_ACTION_ANSWER";
    public static final String CALL_ACTION_DECLINE = "CALL_ACTION_DECLINE";

}
