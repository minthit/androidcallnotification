package com.minthittun.webrtcapp.Receivers;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.minthittun.webrtcapp.MainActivity;
import com.minthittun.webrtcapp.WebRtcCallActivity;


import static com.minthittun.webrtcapp.Helpers.WebRtcAppConstant.CALL_ACTION_ANSWER;
import static com.minthittun.webrtcapp.Helpers.WebRtcAppConstant.CALL_ACTION_DECLINE;
import static com.minthittun.webrtcapp.Helpers.WebRtcAppConstant.NOTIFICATION_CHANNEL_ID;
import static com.minthittun.webrtcapp.Helpers.WebRtcAppConstant.NOTIFICATION_ID;

public class NotificationActionReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        String action=intent.getAction();
        if(action.equals(CALL_ACTION_ANSWER)){
            answerCall(context);
        }
        else if(action.equals(CALL_ACTION_DECLINE)){
            declineCall(context);
        }


    }

    private void answerCall(Context context){

        final NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(NOTIFICATION_ID);

        Intent webRtcIntent = new Intent(context, WebRtcCallActivity.class);
        webRtcIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(webRtcIntent);

    }

    private void declineCall(Context context){

        final NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(NOTIFICATION_ID);

        Intent mainIntent = new Intent(context, MainActivity.class);
        mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(mainIntent);

    }

}
