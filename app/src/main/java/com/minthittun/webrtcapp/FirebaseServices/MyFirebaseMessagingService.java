package com.minthittun.webrtcapp.FirebaseServices;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.PowerManager;
import android.util.Log;
import android.view.WindowManager;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.minthittun.webrtcapp.PhoneNotificationActivity;
import com.minthittun.webrtcapp.R;
import com.minthittun.webrtcapp.Receivers.NotificationActionReceiver;

import java.util.Map;

import static com.minthittun.webrtcapp.Helpers.WebRtcAppConstant.CALL_ACTION_ANSWER;
import static com.minthittun.webrtcapp.Helpers.WebRtcAppConstant.CALL_ACTION_DATA;
import static com.minthittun.webrtcapp.Helpers.WebRtcAppConstant.CALL_ACTION_DECLINE;
import static com.minthittun.webrtcapp.Helpers.WebRtcAppConstant.NOTIFICATION_CHANNEL_ID;
import static com.minthittun.webrtcapp.Helpers.WebRtcAppConstant.NOTIFICATION_CHANNEL_NAME;
import static com.minthittun.webrtcapp.Helpers.WebRtcAppConstant.NOTIFICATION_ID;


public class MyFirebaseMessagingService extends FirebaseMessagingService {



    @SuppressLint("WrongConstant")
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // ...

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d("WebRtcApp", "From: " + remoteMessage.getFrom());



        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d("WebRtcApp", "Message data payload: " + remoteMessage.getData());

            sendNotification(remoteMessage);

            Intent intent = new Intent(getApplicationContext(), PhoneNotificationActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            intent.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED +
                    WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD +
                    WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON +
                    WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
            startActivity(intent);

        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d("WebRtcApp", "Message Notification Body: " + remoteMessage.getNotification().getBody());

        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.


    }

    @Override
    public void onNewToken(String token) {
        Log.d("WebRtcApp", "Refreshed token: " + token);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        //sendRegistrationToServer(token);
    }

    private void sendNotification(RemoteMessage remoteMessage){

        Log.d("WebRtcApp", "Send noti");

        Map<String, String> data = remoteMessage.getData();
        String title = data.get("title");
        String content = data.get("content");

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        int notificationChannelImportance = NotificationManager.IMPORTANCE_MAX;

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            @SuppressLint("WrongConstant")
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, NOTIFICATION_CHANNEL_NAME, notificationChannelImportance);
            notificationChannel.setDescription("FCM Testing");
            notificationChannel.setVibrationPattern(new long[] { 1000, 1000, 1000, 1000, 1000 });
            notificationChannel.enableVibration(true);
            notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);

            notificationManager.createNotificationChannel(notificationChannel);
        }

        Intent fullScreenIntent = new Intent(this, PhoneNotificationActivity.class);
        PendingIntent fullScreenPendingIntent = PendingIntent.getActivity(this, 0,
                fullScreenIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent answerAction = new Intent(this, NotificationActionReceiver.class);
        //answerAction.putExtra(CALL_ACTION_DATA,"Answer");
        answerAction.setAction(CALL_ACTION_ANSWER);
        PendingIntent answerPendingIntent = PendingIntent.getBroadcast(this,100, answerAction, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent declineAction = new Intent(this, NotificationActionReceiver.class);
        //answerAction.putExtra(CALL_ACTION_DATA,"Decline");
        declineAction.setAction(CALL_ACTION_DECLINE);
        PendingIntent declinePendingIntent = PendingIntent.getBroadcast(this, 101, declineAction, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
        notificationBuilder.setAutoCancel(true)
                .setContentTitle(title)
                .setContentText(content)
                .setSmallIcon(R.mipmap.phone_answer)
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setCategory(NotificationCompat.CATEGORY_CALL)
                .setDefaults(Notification.DEFAULT_ALL)
                .setAutoCancel(true)
                .setVibrate(new long[] { 1000, 1000, 1000, 1000, 1000 })
                .setLights(Color.RED, 3000, 3000)
                .setFullScreenIntent(fullScreenPendingIntent, true)
                .setCategory(NotificationCompat.CATEGORY_CALL)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .addAction(R.mipmap.phone_answer, "Answer", answerPendingIntent)
                .addAction(R.mipmap.phone_decline, "Decline", declinePendingIntent);

        notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());

        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        @SuppressLint("InvalidWakeLockTag")
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "WebRtcApp");
        wl.acquire(15000);

    }

    /*private void sendNotification(RemoteMessage remoteMessage){

        Log.d("WebRtcApp", "Send noti");

        Map<String, String> data = remoteMessage.getData();
        String title = data.get("title");
        String content = data.get("content");

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        int notificationChannelImportance = NotificationManager.IMPORTANCE_MAX;

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            @SuppressLint("WrongConstant")
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, NOTIFICATION_CHANNEL_NAME, notificationChannelImportance);
            notificationChannel.setDescription("FCM Testing");
            notificationChannel.setVibrationPattern(new long[] { 1000, 1000, 1000, 1000, 1000 });
            notificationChannel.enableVibration(true);
            notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            notificationChannel.setSound(Uri.parse("android.resource://" + getApplicationContext().getPackageName() + "/" + R.raw.ringtone),
                    new AudioAttributes.Builder().setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                            .setLegacyStreamType(AudioManager.STREAM_RING)
                            .setUsage(AudioAttributes.USAGE_VOICE_COMMUNICATION).build());

            notificationManager.createNotificationChannel(notificationChannel);
        }

        Intent fullScreenIntent = new Intent(this, PhoneNotificationActivity.class);
        PendingIntent fullScreenPendingIntent = PendingIntent.getActivity(this, 0,
                fullScreenIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent mainscreenIntent = new Intent(this, MainActivity.class);
        PendingIntent mainScreenPendingIntent = PendingIntent.getActivity(this, 0,
                mainscreenIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
        notificationBuilder.setAutoCancel(true)
                .setContentTitle(title)
                .setContentText(content)
                .setSmallIcon(R.mipmap.phone_answer)
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setCategory(NotificationCompat.CATEGORY_CALL)
                .setDefaults(Notification.DEFAULT_ALL)
                .setAutoCancel(true)
                .setVibrate(new long[] { 1000, 1000, 1000, 1000, 1000 })
                .setLights(Color.RED, 3000, 3000)
                .setSound(Uri.parse("android.resource://" + getApplicationContext().getPackageName() + "/" + R.raw.ringtone))
                .setFullScreenIntent(fullScreenPendingIntent, true)
                .setCategory(NotificationCompat.CATEGORY_CALL)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .addAction(R.mipmap.phone_answer, "Answer", fullScreenPendingIntent)
                .addAction(R.mipmap.phone_decline, "Decline", mainScreenPendingIntent);

        notificationManager.notify(1, notificationBuilder.build());

        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        @SuppressLint("InvalidWakeLockTag")
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "WebRtcApp");
        wl.acquire(15000);

    }*/


}
